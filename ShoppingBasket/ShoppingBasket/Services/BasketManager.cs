﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using ShoppingBasket.Discounts;
using ShoppingBasket.Models;
using ShoppingBasket.Services.Contracts;

namespace ShoppingBasket.Services
{
    //Note: one instance of BasketManager is connected with one Basket
    public class BasketManager : IBasketManager
    {
        private readonly Basket _basket = new Basket();
        private readonly IProductService _productService;

        public BasketManager(IProductService productService)
        {
            _productService = productService;
        }

        //TODO: add quantity UOM, change type of quantity to double
        public void AddItem(int productId, int quantity)
        {
            var productToAdd = _productService.GetById(productId);
            if (_basket.BasketItems.Any(x => x.ProductId == productId))
                _basket.BasketItems.Find(x => x.ProductId == productId).Quantity += quantity;
            else
                _basket.BasketItems.Add(new BasketItem(productToAdd, quantity));
        }

        public void RemoveItem(int productId)
        {
            var productToRemove = _basket.BasketItems.FirstOrDefault(x => x.Product.Id == productId);
            if (productToRemove == null) return;
            _basket.BasketItems.Remove(productToRemove);
        }

        public void ClearBasket()
        {
            _basket.BasketItems.Clear();
        }

        //Note: discounts must be defined for different type of products
        //If 2 discounts are related to same products calculation will be wrong
        public double GetTotal(List<Discount> discounts)
        {
            var appliedDiscounts = new List<string>();
            var totalDiscounts = discounts.Sum(x =>
            {
                var discount = x.CalculateDiscount(_basket);
                if (discount > 0) appliedDiscounts.Add(x.Name);
                return discount;
            });
            var totalWithDiscount = _basket.Total - totalDiscounts;

            LogBasket(totalWithDiscount, appliedDiscounts);
            return Math.Round(totalWithDiscount, 2);
        }

        //TODO: Move to another service with interface and inject in basket service
        private void LogBasket(double totalWithDiscount, List<string> appliedDiscounts)
        {
            var us = new CultureInfo("en-US");
            
            foreach (var basketItem in _basket.BasketItems)
            {
                System.Diagnostics.Debug.WriteLine($"Product: { basketItem.Product.Name}\t Price: {basketItem.Product.Price.ToString("C", us)}\t Quantity: {basketItem.Quantity}\t ");
            }

            System.Diagnostics.Debug.WriteLine("---------------------------------------------------------------");
            System.Diagnostics.Debug.WriteLine($"Basket total: {totalWithDiscount.ToString("C", us)}");
            appliedDiscounts.ForEach(x => System.Diagnostics.Debug.WriteLine($"Applied discount: {x}"));
        }
    }
}
