﻿using System.Collections.Generic;
using ShoppingBasket.Discounts;

namespace ShoppingBasket.Services.Contracts
{
    public interface IBasketManager
    {
        void AddItem(int productId, int quantity);

        void RemoveItem(int productId);

        void ClearBasket();

        double GetTotal(List<Discount> discount);
    }
}
