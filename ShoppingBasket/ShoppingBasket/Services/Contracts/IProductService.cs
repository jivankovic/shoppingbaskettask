﻿using System.Collections.Generic;
using ShoppingBasket.Models;

namespace ShoppingBasket.Services.Contracts
{
    public interface IProductService
    {
        List<Product> GetAll();
        Product GetById(int id);
    }
}
