﻿using System.Collections.Generic;
using System.Linq;

namespace ShoppingBasket.Models
{
    public class Basket
    {
        public List<BasketItem> BasketItems = new List<BasketItem>();
        public double Total => BasketItems.Sum(x => x.Total);
    }
}
