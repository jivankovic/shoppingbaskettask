﻿namespace ShoppingBasket.Models
{
    public sealed class BasketItem
    {
        public BasketItem(Product product, int quantity)
        {
           Product = product;
           Quantity = quantity;
        }

        public Product Product { get; }
        public int ProductId => Product?.Id ?? 0;
        public int Quantity { get; set; }
        public double Total => Product.Price * Quantity;
    }
}

