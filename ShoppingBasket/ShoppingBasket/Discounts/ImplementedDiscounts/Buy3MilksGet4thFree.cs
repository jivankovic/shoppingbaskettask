﻿using System.Linq;
using ShoppingBasket.Models;

namespace ShoppingBasket.Discounts.ImplementedDiscounts
{
    public sealed class Buy3MilksGet4ThFree : Discount
    {
        public Buy3MilksGet4ThFree(): base("Buy 3 Milks Get 4th Free")
        {
        }

        public override double CalculateDiscount(Basket basket)
        {
            if (basket.BasketItems.FirstOrDefault(x => x.Product.Name.Contains("Milk"))?.Quantity > 3)
            {
                var numberOfMilksForFree = basket.BasketItems.Find(x => x.Product.Name.Contains("Milk")).Quantity / 3;
                var milkPrice = basket.BasketItems.Find(x => x.Product.Name.Contains("Milk")).Product.Price;

                return numberOfMilksForFree * milkPrice;
            }
            return 0;
        }
    }
}
