﻿using System;
using System.Linq;
using ShoppingBasket.Models;

namespace  ShoppingBasket.Discounts.ImplementedDiscounts
{
    public sealed class Buy2ButtersGet1BreadAt50PercentOff : Discount
    {
        public Buy2ButtersGet1BreadAt50PercentOff() : base("Buy 2 Butters Get 1 Bread At 50% Off")
        {
        }

        public override double CalculateDiscount(Basket basket)
        {
            if (basket.BasketItems.FirstOrDefault(x => x.Product.Name.Contains("Butter"))?.Quantity > 1 &&
                basket.BasketItems.Any(x => x.Product.Name.Contains("Bread")))
            {
                var numberOfBreadsToDiscount = basket.BasketItems.Find(x => x.Product.Name.Contains("Butter")).Quantity / 2;
                var breadPrice = basket.BasketItems.Find(x => x.Product.Name.Contains("Bread")).Product.Price;

                return Math.Round(numberOfBreadsToDiscount * breadPrice * 0.5, 2);
            }
            return 0;
        }
    }
}

