﻿using ShoppingBasket.Models;

namespace ShoppingBasket.Discounts
{
    public abstract class Discount
    {
        protected Discount(string name)
        {
            Name = name;
        }

        public virtual string Name { get; }
        public abstract double CalculateDiscount(Basket basket);
    }
}
