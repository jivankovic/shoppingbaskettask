﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using ShoppingBasket.Discounts;
using ShoppingBasket.Discounts.ImplementedDiscounts;
using ShoppingBasket.Models;
using ShoppingBasket.Services;
using ShoppingBasket.Services.Contracts;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace UnitTests
{
    [TestFixture]
    public class ShoppingBasketTests
    {
        private IBasketManager _basketManager;
        private Mock<IProductService> _productServiceMock;

        [SetUp]
        public void SetUp()
        {
            _productServiceMock = new Mock<IProductService>();
            _basketManager = new BasketManager(_productServiceMock.Object);
        }
        
        [Test]
        public void GetTotal_BasketHas2Butters2Breads_TotalShouldBe3_10()
        {
            // arrange  
            _productServiceMock.Setup(x => x.GetById(1))
                .Returns(new Product {Id = 1, Name = "Bread", Price = 1});
            _productServiceMock.Setup(x => x.GetById(2))
                .Returns(new Product { Id = 2, Name = "Butter", Price = 0.8 });

            // act 
            _basketManager.AddItem(1, 2);
            _basketManager.AddItem(2,2);
            var total = _basketManager.GetTotal(new List<Discount>{new Buy2ButtersGet1BreadAt50PercentOff()});

            //assert
            Assert.AreEqual(3.10, total);
        }

        [Test]
        public void GetTotal_BasketHas4Milks_Get4thFree()   
        {
            // arrange  
            _productServiceMock.Setup(x => x.GetById(3))
                .Returns(new Product { Id = 3, Name = "Milk", Price = 1.15 });

            // act 
            _basketManager.AddItem(3, 4);
            var total = _basketManager.GetTotal(new List<Discount>{new Buy3MilksGet4ThFree()});

            //assert
            Assert.AreEqual(3.45, total);
        }

        [Test]
        public void GetBasketTo_BasketHas1Bread1Butter1Milk_TotalShouldBe2_95()
        {
            //arrange
            _productServiceMock.Setup(x => x.GetById(1))
                .Returns(new Product {Id = 1, Name = "Bread", Price = 1});
            _productServiceMock.Setup(x => x.GetById(2))
                .Returns(new Product { Id = 2, Name = "Butter", Price = 0.8 });
            _productServiceMock.Setup(x => x.GetById(3))
                .Returns(new Product { Id = 3, Name = "Milk", Price = 1.15 });

            //act
            _basketManager.AddItem(1, 1);
            _basketManager.AddItem(2, 1);
            _basketManager.AddItem(3, 1);
            var total = _basketManager.GetTotal(new List<Discount>{new Buy2ButtersGet1BreadAt50PercentOff(), new Buy3MilksGet4ThFree()});

            //assert
            Assert.AreEqual(2.95, total);
        }

        [Test]
        public void GetTotal_BasketHas2Butters1Bread8Milks_TotalShouldBe9()
        {
            //arrange
            _productServiceMock.Setup(x => x.GetById(1))
                .Returns(new Product {Id = 1, Name = "Bread", Price = 1});
            _productServiceMock.Setup(x => x.GetById(2))
                .Returns(new Product { Id = 2, Name = "Butter", Price = 0.8 });
            _productServiceMock.Setup(x => x.GetById(3))
                .Returns(new Product { Id = 3, Name = "Milk", Price = 1.15 });

            //act
            _basketManager.AddItem(1,1);
            _basketManager.AddItem(2,2);
            _basketManager.AddItem(3,8);
            var total = _basketManager.GetTotal(new List<Discount>{new Buy2ButtersGet1BreadAt50PercentOff(), new Buy3MilksGet4ThFree()});

            //assert
            Assert.AreEqual(9, total);
        }
    }
}
